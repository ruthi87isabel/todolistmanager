﻿#### Ciklum technical task
Q/A:

 #### How long did you spend on your solution?
 I spent in my solution two days:
 Day 1: 
 - Development environment setup (*Microsoft Visual Studio Community 2017 v15.9.13*): creation of solution and main project (*ASP.NET Core Web Application with React template*), installation of *node.js v10.16.0* with *NPM package*, environment configuration and testing.
 - Implementation of a *web API* with *.NET Core 2.1*.
 - Implementation of a Repository project for data management (*.NET Core 2.1 class library*), added to the solution.
 - Implementation of a test project for the Repository class library (*xUnit test project .NET Core 2.1*).
 - Implementation of a test project for the web API controller (*xUnit test project .NET Core 2.1*).
 - Ensuring all tests (41 units tests in total) pass.
 
Day 2:
 - Implementation of *React* components in the *ASP.NET Core Web Application* (*React v16.0.0*).
 - Ensuring all the required features work properly.
 - Code review and refactoring to ensure clean and quality code.

 #### How do you build and run your solution?
 The solution is built and run in *Microsoft Visual Studio Community 2017* development environment (*target framework .NET Core 2.1*). It is necessary to have a recent version of *node.js* installed for the server to be able to start. *Node.js* also includes *NPM package*. In development environment, the application starts its own instance of the *CRA development server* in the background by automatically calling *npm start* command. 
 The first time oppened, it is mandatory to build the solution for the packages to be restored. The project *TodoListManager.Spa* must be set as startup project.
 
 #### What assumptions did you make when implementing your solution?
The solution was only ran in development environment. Deployment to server wasn't made in order to save time (and because the deployment environment can vary so much that it makes no sense wasting time on that).
Also there was a point of the specification which couldn't be accomplished. The language used for the React components was *JSX syntax JavaScript*, and not *TypeScript* as required (there was not enough time to learn *TypeScript*, and the main goal was to finish the technical task on time).
The main focus of the developer was to provide a clean, clear and simple solution, making use of the correct design patterns, principles and coding practices.
 
 #### Why did you pick the design that you did?
 The solution name is *TodoListManager* and is composed by four (4) projects:
 
  - *TodoListManager.Repository*: 
As the name suggests, the purpose of this library is data handling. Data handling was implemented separated from the SPA for obvious reasons. This library, although the project is called *TodoListManager.Repository* for simplicity purposes (to have all projects names with the format: *[solution_name].[project_purpose]*), behaves like a generic repository, so it could be used to handle any type of data with a minimum conditions.
 This library implements the *Repository Pattern Design*. So there is a generic interface `IDataContext<T> : IEnumerable<T> where T : IDataEntity`, defining that the repository class can be enumerated and can handle any type of data that implements the `IDataEntity`interface. The `IDataEntity` interface defines that the type of data handled by the repository must have an `Id`, and this is the only requirement. This approach provides an abstraction of data that allows re-usability for almost any solution where a data context is needed. This approach also guarantees the decoupling of the different modules composing a solution, as the consuming modules should rely on the repository abstraction, with no implementation details disturbing the quality and usability of the code, and this is how the *Dependency Inversion Principle* is also accomplished.
 For data persistence, there is a `IDataPersistenceService` interface which defines the behavior of the class to be used for this purpose. The implementation provided of `IDataContext<T>` relies on this interface to accomplish that. This is another point in the solution design that enforces the abstraction, this time for the data store implementation. In this case, for simplicity purpose, it was considered to be used a very simple approach for data persistence: a Json file saved in a relative path to ensure this technical task can run properly in any computer (so a `JsonFileService` class is injected as `IDataPersistenceService`). In a real environment this approach should be different. This can be achieved by simply providing the required implementation of `IDataPersistenceService`and injecting this new implementation instead.
 So we get to another remarkable point of the current design: it relies on *Dependency Injection* to handle all dependencies.
 Also it can be noticed that the *data context definition* is separated from the *data store definition*, as the data context only defines functions of reading, adding, updating or deleting entities, while the data store defines functions of retrieving and saving data  "somewhere". This ways, two more of the *SOLID principles* have been fulfilled: the *Single Responsibility Principle* and the *Interface Segregation Principle*.
  
  - *TodoListManager.Repository.Test*:
  As the name suggests, the purpose of this project is to provide unit testing for the *TodoListManager.Repository* project. 19 testing units are available, 8 for the `JsonFileService` class and 11 for the `DataContext<T>` class. Units testing is very important for the solution integrity (robustness, flexibility, maintainability and extensibility), so this is a must in any solution design.
  
  - *TodoListManager.Spa*:
  This is a *ASP.NET Core Web Application* that uses the *React* template, configured as a SPA application. In this project the *Web API* solution is implemented as a *RESTful service* in a `TodoListController` controller class which actions implement the corresponding HTTP methods and URIs to expose the data. The `TodoListController` class relies on an `IDataContext<TodoItem>`as data repository, where `TodoItem` is the class used to model the entities to be handled. 
  The decision of using a *RESTful web API* was very straightforward. The simplicity of the requirements did not lead to major complications, especially considering that *RESTful web APIs* are a very strongly implanted architecture standard.  
  Also this project handles the UI, built on top of the template provided by *Microsoft Visual Studio*. Building the UI on top of this template saves a lot of work, as all that is needed is to implement the desired *React* components and fit them within that template. So, 4 components were implemented: `TaskContainer`, `TaskList`, `Task` and `AddTaskForm`. It has to be said that no extra effort was made in terms of visual design, as it was considered this point of minor priority for the purpose of this technical task. Time was spent mostly on trying to build a clear and simple solution, with clear and clean code.
  
  - *TodoListManager.Spa.Test*:
 As the name suggests, the purpose of this project is to provide unit testing for the *TodoListManager.Spa* project. 22 testing units are available, all of them for the `TodoListController` class. 
