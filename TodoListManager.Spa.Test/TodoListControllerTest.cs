﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TodoListManager.Spa.Controllers;
using TodoListManager.Spa.Models;
using Xunit;

namespace TodoListManager.Spa.Test
{
    public class TodoListControllerTest
    {
        [Fact]
        public void GetItems_EmptyDataContext_ReturnsOkObjectResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItems().Result;

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetItems_EmptyDataContext_NoItemsRetrieved()
        {
            //Arrange
            var dataContext = MockingHelper.GetEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItems().Result as OkObjectResult;

            //Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<TodoItem>>(result.Value);
            Assert.Empty((IEnumerable<TodoItem>)result.Value);
        }

        [Fact]
        public void GetItems__NonEmptyDataContext_ReturnsOkObjectResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItems().Result;

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetItems_NonEmptyDataContext_ItemsRetrieved()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItems().Result as OkObjectResult;

            //Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<TodoItem>>(result.Value);
            Assert.Equal(MockingHelper.TodoItemsList, (IEnumerable<TodoItem>)result.Value);
        }

        [Theory]
        [InlineData("Pending")]
        [InlineData("Completed")]
        public void GetItemsByStatus__ReturnsOkObjectResult(string statusName)
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);
            TodoItemStatus status = (statusName == "Completed" ? TodoItemStatus.Completed : TodoItemStatus.Pending);

            //Act
            var result = controller.GetTodoItems(statusName).Result;

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Theory]
        [InlineData("Pending")]
        [InlineData("Completed")]
        public void GetItemsByStatus__ItemsWithCorrectStatusRetrieved(string statusName)
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);
            TodoItemStatus status = (statusName == "Completed" ? TodoItemStatus.Completed : TodoItemStatus.Pending);

            //Act
            var result = controller.GetTodoItems(statusName).Result as OkObjectResult;

            //Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<TodoItem>>(result.Value);
            Assert.All((IEnumerable<TodoItem>)result.Value, i => Assert.Equal(status, i.State));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        public void GetExistingItemById_ReturnsOkObjectResult(int id)
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItem(id).Result;

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        public void GetExistingItemById_ItemWithCorrectIdRetrieved(int id)
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItem(id).Result as OkObjectResult;

            //Assert
            Assert.NotNull(result);
            Assert.IsType<TodoItem>(result.Value);
            Assert.Equal(id, ((TodoItem)result.Value).Id);
        }

        [Fact]
        public void GetNonexistentItemById_ReturnsNotFoundResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.GetTodoItem(5).Result;

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void PostItem_ReturnsCreatedAtActionResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);
            var itemToPost = new TodoItem()
            {
                Id = 5,
                Title = "Title5",
                Text = "Description5",
                State = TodoItemStatus.Pending
            };

            //Act
            var result = controller.PostTodoItem(itemToPost).Result;

            //Assert
            Assert.IsType<CreatedAtActionResult>(result);
        }

        [Fact]
        public void PutExistingItem_ReturnsNoContentResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);
            var itemToPut = new TodoItem()
            {
                Id = 2,
                Title = "Title2Updated",
                Text = "Description2Updated",
                State = TodoItemStatus.Completed
            };

            //Act
            var result = controller.PutTodoItem(2, itemToPut);

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void PutNonexistentItem_ReturnsBadRequestResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);
            var itemToPut = new TodoItem()
            {
                Id = 5,
                Title = "Title5",
                Text = "Description5",
                State = TodoItemStatus.Pending
            };

            //Act
            var result = controller.PutTodoItem(2, itemToPut);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void DeleteExistingItem_ReturnsNoContentResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.DeleteTodoItem(2);

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void DeleteNonexistentItem_ReturnsNotFoundResult()
        {
            //Arrange
            var dataContext = MockingHelper.GetNonEmptyDataContextMock();
            var controller = new TodoListController(dataContext);

            //Act
            var result = controller.DeleteTodoItem(5);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
