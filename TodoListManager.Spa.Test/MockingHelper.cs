﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TodoListManager.Spa.Models;
using TodoListRepository;

namespace TodoListManager.Spa.Test
{
    public class MockingHelper
    {
        public static List<TodoItem> TodoItemsList => new List<TodoItem>()
        {
            new TodoItem(){Id=1, Title="Title1", Text="Description1", State=TodoItemStatus.Pending},
            new TodoItem(){Id=2, Title="Title2", Text="Description2", State=TodoItemStatus.Pending},
            new TodoItem(){Id=3, Title="Title3", Text="Description3", State=TodoItemStatus.Completed},
            new TodoItem(){Id=4, Title="Title4", Text="Description4", State=TodoItemStatus.Completed}
        };

        public static IDataContext<TodoItem> GetEmptyDataContextMock() => new DataContexMock(new List<TodoItem>());

        public static IDataContext<TodoItem> GetNonEmptyDataContextMock() => new DataContexMock(TodoItemsList);

        internal class DataContexMock : IDataContext<TodoItem>
        {
            private readonly IList<TodoItem> itemsList = new List<TodoItem>();

            public DataContexMock(IList<TodoItem> itemsList) => this.itemsList = itemsList;

            public IEnumerable<TodoItem> GetItems(Func<TodoItem, bool> predicate = null)
                => (predicate != null) ? itemsList.Where(predicate) : itemsList;

            public void AddItem(TodoItem item) => itemsList.Add(item);

            public void UpdateItem(TodoItem item)
            {
                var oldItem = itemsList.SingleOrDefault(i => i.Id == item.Id);
                if (oldItem == null)
                    return;

                int index = itemsList.IndexOf(oldItem);
                itemsList[index] = item;
            }

            public void DeleteItem(TodoItem item) => itemsList.Remove(item);

            public void SaveChanges() { }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<TodoItem> GetEnumerator() => itemsList.GetEnumerator();
        }
    }
}
