﻿namespace TodoListRepository.Services
{
    public interface IDataPersistenceService
    {
        /// <summary>
        /// Retrieves data from a given path or connection string.
        /// </summary>
        /// <typeparam name="T">Type of the data to be retrieved</typeparam>
        /// <param name="path">Path or connection string</param>
        /// <returns>Data to be retrieved, and whether it was successful</returns>
        (T, bool) RetrieveData<T>(string path);

        /// <summary>
        /// Saves data to a given path or connection string.
        /// </summary>
        /// <typeparam name="T">Type of the data to be saved</typeparam>
        /// <param name="value">Object to be saved</param>
        /// <param name="path">Path or connection string</param>
        /// <returns>Whether the object was successfuly saved</returns>
        bool SaveData<T>(T value, string path);
    }
}
