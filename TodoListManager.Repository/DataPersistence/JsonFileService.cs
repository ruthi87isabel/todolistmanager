﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace TodoListRepository.Services
{
    public sealed class JsonFileService : IDataPersistenceService
    {
        public (T, bool) RetrieveData<T>(string path)
        {
            try
            {
                string jsonContent = File.ReadAllText(path, Encoding.UTF8);
                return (JsonConvert.DeserializeObject<T>(jsonContent), true);
            }
            catch (Exception e)
            {
                return (default(T), false);
            }
        }

        public bool SaveData<T>(T value, string path)
        {
            try
            {
                string jsonContent = JsonConvert.SerializeObject(value);
                File.WriteAllText(path, jsonContent, Encoding.UTF8);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
