﻿using System;
using System.Collections.Generic;

namespace TodoListRepository
{
    public interface IDataContext<T> : IEnumerable<T> where T : IDataEntity
    {
        /// <summary>
        /// Retrieves the list of items
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetItems(Func<T, bool> predicate = null);

        /// <summary>
        /// Adds a new item to the list
        /// </summary>
        /// <param name="item"></param>
        void AddItem(T item);

        /// <summary>
        /// Updates an item from the list
        /// </summary>
        /// <param name="item"></param>
        void UpdateItem(T item);

        /// <summary>
        /// Removes an item from the list
        /// </summary>
        /// <param name="itemIndex"></param>
        void DeleteItem(T item);

        /// <summary>
        /// Persist changes in items list
        /// </summary>
        void SaveChanges();
    }
}