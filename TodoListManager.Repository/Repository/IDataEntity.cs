﻿namespace TodoListRepository
{
    public interface IDataEntity
    {
        int Id { get; set; }
    }
}