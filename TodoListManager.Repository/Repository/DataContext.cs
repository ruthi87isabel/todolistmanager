﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TodoListRepository.Services;

namespace TodoListRepository
{
    public sealed class DataContext<T> : IDataContext<T> where T : IDataEntity
    {
        private readonly IDataPersistenceService fileService;
        private readonly string path;
        private IList<T> items;
        private bool itemsLoaded;

        public DataContext(IDataPersistenceService fileService)
        {
            this.fileService = fileService;
            path = "data.json";
            ReadData();
        }

        #region IDataContext implementation

        public IEnumerable<T> GetItems(Func<T, bool> predicate = null)
        {
            if (!itemsLoaded)
                ReadData();

            return (predicate != null) ? items.Where(predicate) : items;
        }

        public void AddItem(T item) => items.Add(item);

        public void UpdateItem(T item)
        {
            var oldItem = items.SingleOrDefault(i => i.Id == item.Id);
            if (oldItem == null) //doesn't exist
                return;

            int index = items.IndexOf(oldItem);
            items[index] = item;
        }

        public void DeleteItem(T item)
        {
            var itemToDelete = items.SingleOrDefault(i => i.Id == item.Id);
            if (itemToDelete == null) //doesn't exist
                return;

            items.Remove(itemToDelete);
        }

        public void SaveChanges() => WriteData();

        #endregion

        #region IEnumerable implementation

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<T> GetEnumerator() => items.GetEnumerator();

        #endregion

        private void ReadData()
        {
            (items, itemsLoaded) = fileService.RetrieveData<IList<T>>(path);

            if (!itemsLoaded)
            {
                items = new List<T>();
                itemsLoaded = true;
            }
        }

        private void WriteData() => fileService.SaveData(items, path);
    }
}