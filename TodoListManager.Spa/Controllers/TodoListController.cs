﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoListManager.Spa.Models;
using TodoListRepository;

namespace TodoListManager.Spa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoListController : Controller
    {
        private readonly IDataContext<TodoItem> context;

        public TodoListController(IDataContext<TodoItem> context)
        {
            this.context = context;
        }

        // GET: api/TodoList
        [HttpGet]
        public ActionResult<IEnumerable<TodoItem>> GetTodoItems()
        {
            var items = context.GetItems();
            return Ok(items);
        }

        // GET api/TodoList/pending
        [HttpGet("{itemStatus}")]
        public ActionResult<IEnumerable<TodoItem>> GetTodoItems(string itemStatus)
        {
            IEnumerable<TodoItem> items;
            if (Enum.TryParse(itemStatus, true, out TodoItemStatus status))
            {
                items = context.GetItems(it => it.State == status);
            }
            else
            {
                items = context.GetItems();
            }
            return Ok(items);
        }

        // GET api/TodoList/5
        [HttpGet("{id:int}")]
        public ActionResult<TodoItem> GetTodoItem(int id)
        {
            var todoItem = context.GetItems(it => it.Id == id).SingleOrDefault();
            if (todoItem == null)
                return NotFound();

            return Ok(todoItem);
        }

        // POST api/TodoList
        [HttpPost]
        public ActionResult<TodoItem> PostTodoItem(TodoItem item)
        {
            context.AddItem(item);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetTodoItem), new { id = item.Id }, item);
        }

        // PUT api/TodoList/5
        [HttpPut("{id}")]
        public IActionResult PutTodoItem(int id, TodoItem item)
        {
            if (id != item.Id)
                return BadRequest();

            context.UpdateItem(item);
            context.SaveChanges();
            return NoContent();
        }

        // DELETE api/TodoList/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTodoItem(int id)
        {
            var item = context.GetItems(it => it.Id == id).SingleOrDefault();
            if (item == null)
                return NotFound();

            context.DeleteItem(item);
            return NoContent();
        }
    }
}
