﻿namespace TodoListManager.Spa.Models
{
    public enum TodoItemStatus
    {
        Pending, Completed
    }
}
