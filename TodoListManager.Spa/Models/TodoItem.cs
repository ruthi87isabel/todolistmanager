﻿using TodoListRepository;

namespace TodoListManager.Spa.Models
{
    public class TodoItem : IDataEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public TodoItemStatus State { get; set; }

        public override bool Equals(object obj)
        {
            TodoItem todoItem = obj as TodoItem;
            if (todoItem == null)
                return false;

            return todoItem.Id == Id &&
                   todoItem.Title == Title &&
                   todoItem.Text == Text &&
                   todoItem.State == State;
        }
    }
}
