﻿import React, { Component } from 'react';
import { Form, FormGroup, FormControl, Button, Panel } from 'react-bootstrap';

export class AddTaskForm extends Component {
    displayName = AddTaskForm.name

    constructor(props) {
        super(props);
        this.state = { title: "", text: "" };
    }

    taskContentChanged = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({ [name]: value });
    }

    submitNewTask = (e) => {
        e.preventDefault();
        let newTitle = this.state.title.trim();
        let newText = this.state.text.trim();
        if (!newTitle || !newText) {
            return;
        }
        this.props.addTaskSubmit({ Title: newTitle, Text: newText });
        this.setState({ title: "", text: "" });
    }

    render() {
        return (
            <div>
                <br />
                <Panel className="newTask" header="New Task" collapsible>
                    <Form noValidate onSubmit={this.submitNewTask}>
                        <FormGroup>
                            <FormControl type="text" name="title" placeholder="Enter title" onChange={this.taskContentChanged}
                                value={this.state.title} />
                        </FormGroup>
                        <FormGroup>
                            <FormControl type="textarea" name="text" as="textarea" placeholder="Enter description" rows="10"
                                value={this.state.text} onChange={this.taskContentChanged} />
                        </FormGroup>
                        <Button variant="primary" type="submit">Add Task</Button>
                    </Form>
                </Panel>
            </div>
        );
    }
}