﻿import React, { Component } from 'react';
import { Task } from './Task';

export class TaskList extends Component {
    displayName = TaskList.name

    constructor(props) {
        super(props);
    }

    changeTaskStateSubmit = (taskChange) => {
        this.props.changeTaskStateSubmit(taskChange);
    }

    render() {
        const tasks = this.props.data.map(task => (
            <Task key={task.id} id={task.id} title={task.title} text={task.text} state={task.state}
                changeTaskStateSubmit={this.changeTaskStateSubmit}>
            </Task>
        ));
        return <div className="tasksList" >{tasks}</div>;
    };
}