﻿import React, { Component } from 'react';
import { DropdownButton, MenuItem, Panel } from 'react-bootstrap';

const pendingState = 0;
const completeState = 1;
const deleteState = 2;
const pendingStateStyle = "warning";
const completeStateStyle = "success";

export class Task extends Component {
    displayName = Task.name

    constructor(props) {
        super(props);
        this.state = { id: 0, title: "", text: "", state: 0 };
    }

    componentDidMount() {
        this.setState({ id: this.props.id, title: this.props.title, text: this.props.text, state: this.props.state });
    }

    selectionChanged = (eventKey) => {
        this.setState({
            state: eventKey
        });
        this.props.changeTaskStateSubmit({ Id: this.state.id, State: eventKey });
    }

    render() {
        let ddbStyle = this.state.state == pendingState ? pendingStateStyle : completeStateStyle;
        let ddbTitle = this.state.state ? "Completed" : "Pending";

        return (
            <Panel className="taskBody" header={this.state.title} collapsible defaultExpanded>
                <p>{this.state.text}</p>
                <DropdownButton id="stateSelectorddb" key="stateSelector" title={ddbTitle} bsStyle={ddbStyle} bsSize="small"
                    onSelect={this.selectionChanged} pullRight={true}>
                    <MenuItem eventKey={pendingState} active={this.state.state == pendingState}>set pending</MenuItem>
                    <MenuItem eventKey={completeState} active={this.state.state == completeState}>set completed</MenuItem>
                    <MenuItem eventKey={deleteState}>delete task</MenuItem>
                </DropdownButton>                
            </Panel>
        );
    };
}