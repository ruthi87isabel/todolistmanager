﻿import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import { TaskList } from './TasksList';
import { AddTaskForm } from './AddTaskForm';

const pendingState = 0;
const completeState = 1;

export class TasksContainer extends Component {
    displayName = TasksContainer.name

    constructor(props) {
        super(props);
        this.state = { taskItems: [], loading: true };

        this.getTasks();
    }

    getTasks() {
        fetch('api/todolist')
            .then(response => response.json())
            .then(data => {
                this.setState({ taskItems: data, loading: false });
            });
    }

    addTask = (newTask) => {
        let taskId = this.state.taskItems.length + 1;
        let taskToAdd = { Id: taskId, Title: newTask.Title, Text: newTask.Text, State: pendingState }

        fetch("/api/todolist",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(taskToAdd)
            })
            .then(response => response.json())
            .then(data => this.getTasks());
    }

    changeTaskState = (taskChange) => {
        let taskToChange = this.state.taskItems.find(item => item.id == taskChange.Id)
        taskToChange.state = taskChange.State;

        fetch("/api/todolist/" + taskToChange.id,
            {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(taskToChange)
            })
            .then(data => this.getTasks());
    }

    static renderTaskItems(taskItems, onChangeTaskState) {
        let pendingTasks = taskItems.filter(item => item.state == pendingState);
        let completedTasks = taskItems.filter(item => item.state == completeState);
        return (
            <Grid fluid>
                <Row>
                    <Col md={6}>
                        <div className="pendingTasksContainer">
                            <h4> Pending Tasks </h4>
                            <TaskList data={pendingTasks} changeTaskStateSubmit={onChangeTaskState} />
                        </div>
                    </Col>
                    <Col md={6}>
                        <div className="completedTasksContainer">
                            <h4>Completed Tasks</h4>
                            <TaskList data={completedTasks} changeTaskStateSubmit={onChangeTaskState} />
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : TasksContainer.renderTaskItems(this.state.taskItems, this.changeTaskState);

        return (
            <div>
                <div>
                    <AddTaskForm addTaskSubmit={this.addTask}></AddTaskForm>
                </div>
                <div>
                    {contents}
                </div>
            </div>
        );
    }
}