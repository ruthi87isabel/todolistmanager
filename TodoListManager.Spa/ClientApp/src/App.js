import React, { Component } from 'react';
import { Route } from 'react-router';
import { Col, Grid, Row } from 'react-bootstrap';
import { TasksContainer } from './components/TasksContainer';

export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <Grid>
                <Route exact path='/' component={TasksContainer} />
            </Grid>
        );
    }
}