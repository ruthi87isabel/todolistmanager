﻿using NSubstitute;
using TodoListRepository;
using TodoListRepository.Services;

namespace TodoListRepositoryTest
{
    public class MockingHelper
    {
        public static IDataPersistenceService GetDataPersistenceServiceMock<T>()
        {
            var dataPersistenceServiceMock = Substitute.For<IDataPersistenceService>();
            dataPersistenceServiceMock.SaveData<T>(Arg.Any<T>(), Arg.Any<string>()).Returns(true);
            dataPersistenceServiceMock.RetrieveData<T>(Arg.Any<string>()).Returns((default(T), true));
            return dataPersistenceServiceMock;
        }

        public static EntityMock EntityMock1 => new EntityMock(1, 1);
        public static EntityMock EntityMock2 => new EntityMock(2, 2);
        public static EntityMock EntityMock3 => new EntityMock(3, 3);
        public static EntityMock EntityMock4 => new EntityMock(4, 4);
        public static EntityMock EntityMock5 => new EntityMock(5, 5);
    }

    public class EntityMock : IDataEntity
    {
        public EntityMock(int id, object data)
        {
            Id = id;
            Data = data;
        }

        public int Id { get; set; }

        public object Data { get; set; }
    }
}