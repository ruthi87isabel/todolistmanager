﻿using Xunit;
using System.IO;
using TodoListRepository.Services;

namespace TodoListRepositoryTest
{
    public class JsonFileServiceTest
    {
        [Theory]
        [InlineData(new int[] { 1, 2, 3 })]
        [InlineData(new object[] { "item1", "item2", "item3" })]
        public void WriteData_RightJsonFilePath_DataIsWrittenSuccessfully(params object[] itemsList)
        {
            //Arrange
            var jsonFileService = new JsonFileService();
            string path = "testdata.json";

            //Act
            bool success = jsonFileService.SaveData(itemsList, path);

            //Assert
            Assert.True(success);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3 })]
        [InlineData(new object[] { "item1", "item2", "item3" })]
        public void WriteData_WrongJsonFilePath_DataIsNotWritten(params object[] itemsList)
        {
            //Arrange
            var jsonFileService = new JsonFileService();
            string path = Path.Combine("Unexisting", "Json", "Path", "testdata.json");

            //Act
            bool success = jsonFileService.SaveData(itemsList, path);

            //Assert
            Assert.False(success);
        }

        [Theory]
        [InlineData(new object[] { 0.1, 0.2, 0.3 })]
        [InlineData(new object[] { "item1", "item2", "item3" })]
        public void ReadData_RightJsonFilePath_DataIsReadSuccessfully(params object[] itemsList)
        {
            //Arrange
            var jsonFileService = new JsonFileService();
            string path = "testdata.json";
            bool success = jsonFileService.SaveData(itemsList, path);

            //Act
            (object[] items, bool itemsLoaded) = jsonFileService.RetrieveData<object[]>(path);

            //Assert
            Assert.True(itemsLoaded);
            Assert.Equal(itemsList, items);
        }

        [Theory]
        [InlineData(new object[] { 0.1, 0.2, 0.3 })]
        [InlineData(new object[] { "item1", "item2", "item3" })]
        public void ReadData_WrongJsonFilePath_DataIsNotRead(params object[] itemsList)
        {
            //Arrange
            var jsonFileService = new JsonFileService();
            string path = Path.Combine("Unexisting", "Json", "Path", "testdata.json");

            //Act
            (object[] items, bool itemsLoaded) = jsonFileService.RetrieveData<object[]>(path);

            //Assert
            Assert.False(itemsLoaded);
            Assert.Equal(default(object[]), items);
        }
    }
}