﻿using Xunit;
using System.Linq;
using TodoListRepository;

namespace TodoListRepositoryTest
{
    public class DataContextTest
    {
        [Fact]
        public void GetItems_EmptyDataContext_NoItemsRetrieved()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);

            //Act
            var items = context.GetItems();

            //Assert
            Assert.Empty(items);
        }

        [Fact]
        public void GetItems_NonEmptyDataContext_ItemsCountIsOk()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            var items = context.GetItems();
            int count = items.Count();

            //Assert
            Assert.Equal(3, count);
        }

        [Fact]
        public void GetItems_NonEmptyDataContext_ItemsRetrieved()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.Contains(MockingHelper.EntityMock1.Data, items);
            Assert.Contains(MockingHelper.EntityMock2.Data, items);
            Assert.Contains(MockingHelper.EntityMock3.Data, items);
        }

        [Fact]
        public void GetFilteredItems_NonEmptyDataContext_ItemsCountIsOk()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);
            context.AddItem(MockingHelper.EntityMock4);
            context.AddItem(MockingHelper.EntityMock5);

            //Act
            var items = context.GetItems(i => i.Id > 2);
            int count = items.Count();

            //Assert
            Assert.Equal(3, count);
        }

        [Fact]
        public void GetFilteredItems_NonEmptyDataContext_ItemsRetrieved()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);
            context.AddItem(MockingHelper.EntityMock4);
            context.AddItem(MockingHelper.EntityMock5);

            //Act
            var items = context.GetItems(i => i.Id > 2);

            //Assert
            Assert.Contains(items, i => i.Id > 2);
            Assert.DoesNotContain(items, i => i.Id <= 2);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3 })]
        [InlineData(new object[] { "item1", "item2", "item3" })]
        public void AddItems_ItemsAdded(params object[] itemsList)
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            int index = 1;
            foreach (object item in itemsList)
            {
                context.AddItem(new EntityMock(index, item));
                index++;
            }

            //Act
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.All(items, i => Assert.Contains(i, itemsList));
        }

        [Fact]
        public void UpdateExistingItem_ItemUpdated()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            context.UpdateItem(new EntityMock(2, 4));
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.Contains(4, items);
            Assert.DoesNotContain(2, items);
        }

        [Fact]
        public void UpdateNonexistentItem_NoItemUpdated()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            context.UpdateItem(new EntityMock(5, 4));
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.Equal(new object[] { 1, 2, 3 }, items);
        }

        [Fact]
        public void DeleteExistingItem_ItemDeleted()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            context.DeleteItem(MockingHelper.EntityMock2);
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.Equal(new object[] { 1, 3 }, items);
        }

        [Fact]
        public void DeleteNonexistentItem_NoItemDeleted()
        {
            //Arrange
            var dataPersistenceServiceMock = MockingHelper.GetDataPersistenceServiceMock<EntityMock>();
            var context = new DataContext<EntityMock>(dataPersistenceServiceMock);
            context.AddItem(MockingHelper.EntityMock1);
            context.AddItem(MockingHelper.EntityMock2);
            context.AddItem(MockingHelper.EntityMock3);

            //Act
            context.DeleteItem(new EntityMock(4, 4));
            var items = context.GetItems().Select(i => i.Data);

            //Assert
            Assert.Equal(new object[] { 1, 2, 3 }, items);
        }
    }
}